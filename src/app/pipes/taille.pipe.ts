import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'taille'
})
export class TaillePipe implements PipeTransform {

  transform(value: unknown[]): number {
    return value.length;
  }

}
