export const UserEndpoint = {
    LOGIN :  "/api/v1/auths/login",
    REGISTER :  "/api/v1/auths/register",
    GET_ALL_USERS :  "/api/v1/auths/all",
    GET_USER :  "/api/v1/auths/one/",
    DELETE_USER : "/api/v1/auths/delete/",
}

export const QuestionEndpoint = {
    CREATE_QUESTION : "/api/v1/question/create",
    ALL_QUESTION : "/api/v1/question/all",
    ONE_QUESTION : "/api/v1/question/one/{id}",
    DELETE_QUESTION : "/api/v1/question/delete/{id}",
}

export const QuizEndpoint = {
    CREATE_QUIZ : "/api/v1/quiz/create",
    ALL_QUIZ : "/api/v1/quiz/all",
    ONE_QUIZ : "/api/v1/quiz/one/{id}",
    DELETE_QUIZ : "/api/v1/quiz/delete/{id}",
}

export const ParticipationEndpoint = {
    CREATE_PARTICIPATION : "/api/v1/participation/create",
    ALL_PARTICIPATION : "/api/v1/participation/all",
    ONE_PARTICIPATION : "/api/v1/participation/one/{id}",
    QUIZ_PARTICIPATION : "/api/v1/participation/quiz",
    DELETE_PARTICIPATION : "/api/v1/participation/delete/{id}",
}