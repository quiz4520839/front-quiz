import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/auths/login/login.component';
import { RegisterComponent } from './pages/auths/register/register.component';
import { HeaderComponent } from './compomemts/header/header.component';
import { FooterComponent } from './compomemts/footer/footer.component';
import { AssidesComponent } from './pages/assides/assides.component';
import { MainComponent } from './pages/main/main.component';
import { QuizComponent } from './pages/quiz/quiz.component';
import { FilterComponent } from './pages/quiz/filter/filter.component';
import { ListQuizComponent } from './pages/quiz/list-quiz/list-quiz.component';
import { NewQuizComponent } from './pages/quiz/new-quiz/new-quiz.component';
import { DetailsQuizComponent } from './pages/quiz/details-quiz/details-quiz.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EvaluationComponent } from './pages/evaluation/evaluation.component';
import { ListEvaluationComponent } from './pages/list-evaluation/list-evaluation.component';
import { QuestionComponent } from './pages/question/question.component';
import { UsersComponent } from './pages/users/users.component';
import { ProfilesComponent } from './pages/profiles/profiles.component';
import { TaillePipe } from './pipes/taille.pipe';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    AssidesComponent,
    MainComponent,
    QuizComponent,
    FilterComponent,
    ListQuizComponent,
    NewQuizComponent,
    DetailsQuizComponent,
    EvaluationComponent,
    ListEvaluationComponent,
    QuestionComponent,
    UsersComponent,
    ProfilesComponent,
    TaillePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
