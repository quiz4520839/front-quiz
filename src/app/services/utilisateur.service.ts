import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Utilisateur } from '../models/Utilisateur';
import { environment } from 'src/environments/environment';
import { UserEndpoint } from '../utils/Endpoint';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  access = JSON.parse(sessionStorage.getItem("userConnected")+"")
  private option: any

  constructor(private readonly http: HttpClient) {  
    if (sessionStorage.getItem("userConnected") != undefined) {
      this.option = {headers: {Authorization: `Bearer ${this.access.token}`}}
    }
    
  }

  login(user: Utilisateur): Observable<Utilisateur>{
    return this.http.post<Utilisateur>(environment.apiUrl + UserEndpoint.LOGIN, user);
  }

  register(user: Utilisateur) : Observable<Utilisateur>{
    return this.http.post<Utilisateur>(environment.apiUrl + UserEndpoint.REGISTER, user);
  }

  getUserById(id: number): Observable<any>{
    return this.http.get<any>(environment.apiUrl + UserEndpoint.GET_USER+id, this.option)
  }

  allusers(): Observable<any>{
    return this.http.get<any>(environment.apiUrl + UserEndpoint.GET_ALL_USERS, this.option)
  }
}
