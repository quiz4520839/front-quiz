import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Questionaire } from '../models/Questionaire';
import { Observable } from 'rxjs';
import { QuizEndpoint } from '../utils/Endpoint';

@Injectable({
  providedIn: 'root'
})
export class QuestionaireService {

  access = JSON.parse(sessionStorage.getItem("userConnected")+"")
  private option: any

  constructor(private http: HttpClient) {
    if (sessionStorage.getItem("userConnected") != undefined) {
      this.option = {headers: {Authorization: `Bearer ${this.access.token}`}}
    }
  }

  saveQuiz(questionaire: Questionaire): Observable<any>{
    return this.http.post<Questionaire>(environment.apiUrl + QuizEndpoint.CREATE_QUIZ, questionaire, this.option);
  }

  oneQuiz(id: number) : Observable<any>{
    return this.http.get<any>(environment.apiUrl + QuizEndpoint.ONE_QUIZ, this.option);
  }

  allQuiz(direction: string, key: string, page: number, elementPerPage: number) : Observable<any>{
    return this.http.get<any>(environment.apiUrl + QuizEndpoint.ALL_QUIZ+`?direction=${direction}&key=${key}&page=${page}&elementPerPage=${elementPerPage}`, this.option)
      
  }


}
