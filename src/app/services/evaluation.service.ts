import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Evaluation } from '../models/Evaluation';
import { Questionaire } from '../models/Questionaire';
import { ParticipationEndpoint } from '../utils/Endpoint';

@Injectable({
  providedIn: 'root'
})
export class EvaluationService {

  access = JSON.parse(sessionStorage.getItem("userConnected")+"")
  private option: any


  constructor(private http: HttpClient) { 
    if (sessionStorage.getItem("userConnected") != undefined) {
      this.option = {headers: {Authorization: `Bearer ${this.access.token}`}}
    }
  }

  saveEvaluation(evaluation: Evaluation): Observable<any>{
    return this.http.post<Evaluation>(environment.apiUrl + ParticipationEndpoint.CREATE_PARTICIPATION, evaluation, this.option);
  }

  evaluationById(id: number) : Observable<any>{
    return this.http.get<Evaluation[]>(environment.apiUrl + ParticipationEndpoint.ONE_PARTICIPATION+id, this.option);
  }

  evaluationByQuizId(questionaire: Questionaire) : Observable<any>{
    return this.http.post<Evaluation[]>(environment.apiUrl + ParticipationEndpoint.QUIZ_PARTICIPATION,questionaire, this.option);
  }

  allEvaluations() : Observable<any>{
    return this.http.get<Evaluation[]>(environment.apiUrl + ParticipationEndpoint.ALL_PARTICIPATION, this.option);
  }
}
