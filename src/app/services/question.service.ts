import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Question } from '../models/Question';
import { Observable } from 'rxjs';
import { QuestionEndpoint } from '../utils/Endpoint';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  access = JSON.parse(sessionStorage.getItem("userConnected")+"")
  private option: any

  constructor(private http: HttpClient) {
    if (sessionStorage.getItem("userConnected") != undefined) {
      this.option = {headers: {Authorization: `Bearer ${this.access.token}`}}
    }
  }

  saveQuestion(question: Question): Observable<any>{
    return this.http.post<Question>(environment.apiUrl + QuestionEndpoint.CREATE_QUESTION, question, this.option);
  }

  oneQuestion(id: number) : Observable<any>{
    return this.http.get<any>(environment.apiUrl + QuestionEndpoint.ONE_QUESTION, this.option);
  }

  allQuestions() : Observable<any>{
    return this.http.get<any>(environment.apiUrl + QuestionEndpoint.ALL_QUESTION, this.option);
  }

}
