import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/auths/login/login.component';
import { RegisterComponent } from './pages/auths/register/register.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { QuizComponent } from './pages/quiz/quiz.component';
import { NewQuizComponent } from './pages/quiz/new-quiz/new-quiz.component';
import { DetailsQuizComponent } from './pages/quiz/details-quiz/details-quiz.component';
import { EvaluationComponent } from './pages/evaluation/evaluation.component';
import { ListEvaluationComponent } from './pages/list-evaluation/list-evaluation.component';
import { ProfilesComponent } from './pages/profiles/profiles.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'register', component: RegisterComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'quiz', component: QuizComponent},
  {path: 'create-quiz', component: NewQuizComponent},
  {path: 'details-quiz', component: DetailsQuizComponent},
  {path: 'create-evaluation', component: EvaluationComponent},
  {path: 'list-evaluation', component: ListEvaluationComponent},
  {path: 'profiles', component: ProfilesComponent},
  {path: 'users', component: UsersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
