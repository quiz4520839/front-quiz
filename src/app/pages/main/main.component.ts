import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js/auto';
import { Evaluation } from 'src/app/models/Evaluation';
import { Question } from 'src/app/models/Question';
import { Questionaire } from 'src/app/models/Questionaire';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { EvaluationService } from 'src/app/services/evaluation.service';
import { QuestionService } from 'src/app/services/question.service';
import { QuestionaireService } from 'src/app/services/questionaire.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit{

  users!: Utilisateur[]
  participations!: Evaluation[]
  quiz!: any
  questions!: Question[]

  constructor(
    private us: UtilisateurService,
    private quizservice: QuestionaireService,
    private qs: QuestionService,
    private es:EvaluationService){

  }

  ngOnInit(): void {
    this.line();
    this.bar()
    this.allUsers()
    this.allQuiz()
    this.allParticipations()
    this.allQuestion()
  }

  private allUsers(){
    this.us.allusers().subscribe(response =>{this.users=response})
  }

  private allQuiz(){
    this.quizservice.allQuiz("desc","titre",0,2).subscribe(response =>{console.log(response);this.quiz=response})
  }
  private allParticipations(){
    this.es.allEvaluations().subscribe(response =>{this.participations=response})
  }

  private allQuestion(){
    this.qs.allQuestions().subscribe(response =>{this.questions=response})
  }

  private line(){
    const line = new Chart('line', {
      type: 'line',
      data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        datasets: [{
          label: 'My First Dataset',
          data: [65, 36, 68, 40, 70, 22, 22],
          fill: false,
          borderColor: 'rgb(75, 192, 192)',
          cubicInterpolationMode : 'monotone', // default
          tension: 0.1
        }]
      }
    });
  }

  private bar(){
    const line = new Chart('bar', {
      type: 'bar',
      data : {
        labels: [1,2,3,4,5,6],
        datasets: [{
          label: 'My First Dataset',
          data: [65, 59, 80, 81, 56, 55, 40],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)'
          ],
          borderColor: [
            'rgb(255, 99, 132)'
          ],
          borderWidth: 1
        }]
    }})
  }


}
