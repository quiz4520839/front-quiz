import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-assides',
  templateUrl: './assides.component.html',
  styleUrls: ['./assides.component.scss']
})
export class AssidesComponent implements OnInit{


  constructor(private readonly router: Router){}

  ngOnInit(): void {

  }

  managerActiveBtn(event: Event, route: string){
    const li = document.getElementsByClassName("list-group-item") as HTMLCollection;
    let actived = event.target as HTMLLIElement;
    
    console.log(actived);
    
    for (let i = 0; i < li.length; i++) {
      li[i].classList.remove("active")
    }
    
    if (actived.classList.contains("active")) {
      actived.classList.remove("active")
    }else{
      actived.classList.add("active")
    }

    this.router.navigate([route])
    
  }

  logout(){
    sessionStorage.clear()
    this.router.navigate(['login']);
  }

}
