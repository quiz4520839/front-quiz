import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { SweetalertService } from 'src/app/services/sweetalert.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  user: Utilisateur = new Utilisateur();

  constructor(private readonly userService: UtilisateurService,private sa: SweetalertService,private readonly router: Router){}

  ngOnInit(): void {
  }

  login(){
    console.log(this.user);
    
    this.userService.login(this.user).subscribe({
      next: (response) =>{
        console.log(response);
        sessionStorage.setItem("userConnected", JSON.stringify(response));
        this.sa.successfulyToastAlert("User connected successfuly");
        this.router.navigate(['dashboard'])
      },
      error: (e)=>{console.log(e);},
    })
  }


}
