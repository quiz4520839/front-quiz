import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { SweetalertService } from 'src/app/services/sweetalert.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  user: Utilisateur = new Utilisateur();

  constructor(private readonly userService: UtilisateurService,private sa: SweetalertService,private readonly router: Router){}

  register(){
    this.user.username = this.user.prenom;
    this.user.roles = ["mod","user"];
    //console.log(this.user);
    
    this.userService.register(this.user).subscribe({
      next: (response) =>{
        console.log(response);
        this.sa.successfulyToastAlert("User Registry successfuly");
        this.router.navigate(['login'])
      },
      error: (e)=>{console.log(e);},
    })
  }

}
