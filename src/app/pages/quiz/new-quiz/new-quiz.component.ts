import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Questionaire } from 'src/app/models/Questionaire';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { QuestionaireService } from 'src/app/services/questionaire.service';
import { SweetalertService } from 'src/app/services/sweetalert.service';

@Component({
  selector: 'app-new-quiz',
  templateUrl: './new-quiz.component.html',
  styleUrls: ['./new-quiz.component.scss']
})
export class NewQuizComponent implements OnInit{

  quiz: Questionaire = new Questionaire();
  user: Utilisateur = JSON.parse(sessionStorage.getItem("userConnected")+"");
  code: string = "";

  constructor(
    private readonly router: Router, 
    private readonly sweetAlert: SweetalertService,
    private readonly qs: QuestionaireService){}

  ngOnInit(): void {
    this.allQuiz()
    console.log(this.user.id);

  }

  saveQuiz(){

    this.quiz.utilisateur = new Utilisateur()
    this.quiz.utilisateur.id = this.user.id;


    console.log(this.quiz);
    
    this.qs.saveQuiz(this.quiz).subscribe({
      next: (response) => { 
        console.log(response);
        this.sweetAlert.successfulyToastAlert("Quiz are created successfuly");
        this.router.navigate(['quiz']);
        },
      error: (e) => { console.log(e); },
      complete: () =>{ 
        this.router.navigate(['/quiz']);
        console.log("Request completed successfuly!"); 
      }
    })
  }

  private allQuiz(){
    this.qs.allQuiz("dsc","titre",0,2).subscribe({
      next: (response) => { 
        this.quiz.code = "23QZ00" + (Number(response.totalElements) + 1)
        console.log(response.totalElements);  
      },
      error: (e) => { console.log(e); },
      complete: () =>{ console.log("Request completed successfuly!"); }
    })
  }

}
