import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Evaluation } from 'src/app/models/Evaluation';
import { Questionaire } from 'src/app/models/Questionaire';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { EvaluationService } from 'src/app/services/evaluation.service';
import { QuestionaireService } from 'src/app/services/questionaire.service';
import { SweetalertService } from 'src/app/services/sweetalert.service';

@Component({
  selector: 'app-list-quiz',
  templateUrl: './list-quiz.component.html',
  styleUrls: ['./list-quiz.component.scss']
})
export class ListQuizComponent implements OnInit{
  
  @Input()
  quizList!: Questionaire[];

  participation: Evaluation = new Evaluation();

  userConnected = JSON.parse(sessionStorage.getItem("userConnected")+"")

  @Input()
  totalElements!: number;

  currentItem: number = 0;
  tab = [1,2,3,4,5]

  constructor(
    private readonly router: Router, 
    private readonly evaluationService: EvaluationService,
    private readonly sweetAlertService: SweetalertService,
    private readonly qs: QuestionaireService){

  }

  ngOnInit(): void {
    this.getAllQuiz("dsc","titre",0,2)
  }

  private getAllQuiz(direction: string, key: string, page: number, elementPerPage: number){
    this.qs.allQuiz(direction, key, page, elementPerPage).subscribe({
      next: (response) => { 
        this.quizList = response.content;
        this.totalElements = response.totalElements;
        console.log(response);  
      },
      error: (e) => { console.log(e); },
      complete: () =>{ console.log("Request completed successfuly!"); }
    })
  }

  goToDetails(quiz: Questionaire){
    this.router.navigate(['details-quiz'], {queryParams: {quizId: quiz.id}})
  }

  next(){
    this.currentItem += 1;
    this.getAllQuiz("dsc","titre",this.currentItem,2)
    this.tab.forEach(item =>{
      item += 1;
    })
  }

  previous(){
    if (this.currentItem >= 1) {
      this.currentItem -= 1;
      this.getAllQuiz("dsc","titre",this.currentItem,2)
      this.tab.forEach(item =>{
        item -= 1;
      })
    }
  }

  createParticipation(quiz: Questionaire){
    console.log(this.userConnected.id);
    
    if (this.userConnected == undefined) {
      this.sweetAlertService.InfoToastAlert("Start connected to Participe for the Quiz!")
      this.router.navigate(['login'])
    }else{
      this.participation.dateEval = new Date();
      this.participation.utilisateur = new Utilisateur()
      this.participation.utilisateur.id = this.userConnected.id;
      this.participation.questionaire = new Questionaire()
      this.participation.questionaire.id = quiz.id;
      this.participation.getIdentify();

      this.evaluationService.saveEvaluation(this.participation).subscribe({
        next: (response) =>{
          console.log(response);
          this.router.navigate(['create-evaluation'], {queryParams: {participationId: response.id}})
          this.sweetAlertService.successfulyToastAlert("Your quiz zas saved successfly!")
        },
        error: (e) => {
          this.sweetAlertService.errorToastAlert("To replay this quiz, go to your quiz list")
          console.log(e);
        }
      })
      console.log(this.participation);
    }
    
  
  }


}
