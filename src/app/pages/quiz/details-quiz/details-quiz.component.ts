import { Component, OnInit  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from 'src/app/models/Question';
import { Questionaire } from 'src/app/models/Questionaire';
import { Response } from 'src/app/models/Response';
import { QuestionService } from 'src/app/services/question.service';
import { QuestionaireService } from 'src/app/services/questionaire.service';

@Component({
  selector: 'app-details-quiz',
  templateUrl: './details-quiz.component.html',
  styleUrls: ['./details-quiz.component.scss']
})
export class DetailsQuizComponent implements OnInit{

  quizId!: number;
  quiz!: Questionaire;

  question: Question = new Question();
  reponse1: Response = new Response(1);
  reponse2: Response = new Response(2);

  constructor(
    private readonly activedRouter: ActivatedRoute, 
    private readonly router: Router, 
    private readonly quizService: QuestionaireService,
    private readonly questionService: QuestionService
    ){}

  ngOnInit(): void {
   this.quizId = Number( this.activedRouter.snapshot.queryParams["quizId"]);
    
    this.getQuiz()
    this.initializeResponseChoice()
  }

  getQuiz(){
    this.quizService.oneQuiz(this.quizId).subscribe({
      next: (response) => { 
        this.quiz = response;
        console.log(response);  
      },
      error: (e) => { console.log(e); },
      complete: () =>{ console.log("Request completed successfuly!"); }
    })
  }

  addQuestion(){
    this.question.questionaire = new Questionaire()
    this.question.questionaire.id = this.quizId;
    
    this.questionService.saveQuestion(this.question).subscribe({
      next: (response) => { 
        this.ngOnInit()
        console.log(response);  
      },
      error: (e) => { console.log(e); },
      complete: () =>{ console.log("Request completed successfuly!"); }
    })
  }

  private initializeResponseChoice(){
    if(this.question.responses.length == 0){
      this.question.responses.push(this.reponse1)
      this.question.responses.push(this.reponse2)
    }
  }

  getResponse(event: Event, response: Response){
    const exactly = event.target as HTMLInputElement;
    if (exactly.value == "on") {
      this.question.responses.forEach((reponse: Response) =>{
        if(response.id == reponse.id){
          reponse.exactly = true;
        }else{
          reponse.exactly = false
        }
      })
    }
  }

  add(){
    if(this.question.responses.length <= 4){
      this.question.responses.push(new Response(this.question.responses.length+1))
    }
  }

}
