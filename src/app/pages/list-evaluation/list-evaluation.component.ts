import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Evaluation } from 'src/app/models/Evaluation';
import { Questionaire } from 'src/app/models/Questionaire';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { EvaluationService } from 'src/app/services/evaluation.service';
import { QuestionaireService } from 'src/app/services/questionaire.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-list-evaluation',
  templateUrl: './list-evaluation.component.html',
  styleUrls: ['./list-evaluation.component.scss']
})
export class ListEvaluationComponent implements OnInit{

  participations!: Evaluation[];
  
  constructor(
    private evaluationService: EvaluationService, 
    private utilisateurService: UtilisateurService,
    private quizService: QuestionaireService,
    private router: Router){}
  
  ngOnInit(): void {
    this.getAllParticipation()
  }

  private getAllParticipation(){
    this.evaluationService.allEvaluations().subscribe({
      next: (response: Evaluation[]) =>{
        response.forEach(part =>{
          this.getUser(Number(part.identify.split('-')[0]), part);
          this.getQuiz(Number(part.identify.split('-')[1]), part);
        })
        this.participations = response;
        console.log(response);
      }, error: (e) =>{
        console.log(e);
        
      }
    })
  }

  private getUser(id: number, evaluation: Evaluation){
    this.utilisateurService.getUserById(id).subscribe({
      next: (userData: Utilisateur) => {
        evaluation.utilisateur = userData;
      },
      error: (e) => {
        console.log(e);
      }
    })
  }

  private getQuiz(id: number, evaluation: Evaluation){
    this.quizService.oneQuiz(id).subscribe({
      next: (quizData: Questionaire) => {
        evaluation.questionaire = quizData;
      },
      error: (e) => {
        console.log(e);
      }
    })
  }

  goToParticipation(participation: Evaluation){
    this.router.navigate(['create-evaluation'], {queryParams : {participationId: participation.id}})
  }

}
