import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Evaluation } from 'src/app/models/Evaluation';
import { Question } from 'src/app/models/Question';
import { Questionaire } from 'src/app/models/Questionaire';
import { Response } from 'src/app/models/Response';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { EvaluationService } from 'src/app/services/evaluation.service';
import { QuestionaireService } from 'src/app/services/questionaire.service';
import { SweetalertService } from 'src/app/services/sweetalert.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-evaluation',
  templateUrl: './evaluation.component.html',
  styleUrls: ['./evaluation.component.scss']
})
export class EvaluationComponent implements OnInit{

  participation: Evaluation = new Evaluation();
  user!: Utilisateur;
  questionaire! : Questionaire;
  currentQuestion:number = 0;
  participantQuizListe!: Evaluation[]
  end: boolean = false;

  userResponses: Response[] = [];
  currentResponse: Response = new Response(0);

  participationId: any;
  private score: number = 0;

  constructor(
    private evaluationService: EvaluationService, 
    private utilisateurService: UtilisateurService,
    private quizService: QuestionaireService,
    private activedRoute: ActivatedRoute){}

  ngOnInit(): void {
    this.participationId = Number( this.activedRoute.snapshot.queryParams["participationId"]);
    this.getEvaluation()
  }

  getSelectedValue(event: Event){
    const reponse = event.target as HTMLInputElement;
    this.currentResponse = new Response(Number(reponse.id));
    this.currentResponse.libelle = reponse.value;
  }

  private getCorrection(){
    this.participation.questionaire.questions[this.currentQuestion].responses.forEach(response =>{
      if (response.exactly) {
        console.log(response);
        
        if (response.id == this.currentResponse.id && response.libelle == this.currentResponse.libelle) {
          this.score += 1;
        }
      }
    });
  }

  nextQuestion(){
      if (this.currentResponse.id != 0) {
        this.getCorrection();
        
        this.userResponses.push(this.currentResponse);
        this.currentQuestion += 1;
        this.currentResponse = new Response(0);
      }
      if(this.participation.questionaire.questions.length == this.currentQuestion+1){
        this.end = true;
      }
  }
  
  terminer(){
    
    if (this.currentResponse.id != 0) {
      this.getCorrection();
      this.userResponses.push(this.currentResponse);
    }
    this.participation.score = this.score
    console.log(this.participation);
    this.evaluationService.saveEvaluation(this.participation).subscribe({
      next: (response =>{
        console.log(response);
        this.ngOnInit()
      })
    })

  }

  getEvaluation(){
    this.evaluationService.evaluationById(this.participationId).subscribe({
      next: (response: Evaluation) =>{
        const quizID = Number(response.identify.split('-')[1])
        const userID = Number(response.identify.split('-')[0])
        this.participation = response;
        this.getUser(userID, this.participation);
        this.getQuiz(quizID);
        console.log(this.participation);
      },
      error: (e) =>{
        console.log(e);
      }
    })
  }

  private getUser(id: number, evaluation: Evaluation){
    this.utilisateurService.getUserById(id).subscribe({
      next: (userData: Utilisateur) => {
        evaluation.utilisateur = userData;
      },
      error: (e) => {
        console.log(e);
      }
    })
  }

  private getQuiz(id: number){
    this.quizService.oneQuiz(id).subscribe({
      next: (quizData: Questionaire) => {
        this.participation.questionaire = quizData;
        this.allParticipationQuiz(quizData)
      },
      error: (e) => {
        console.log(e);
      }
    })
  }

  private allParticipationQuiz(questionaire: Questionaire){
    this.evaluationService.evaluationByQuizId(questionaire).subscribe({
      next: (response: Evaluation[]) =>{
        this.participantQuizListe = response;
        response.forEach(part =>{
          const userID = Number(part.identify.split('-')[0])
          this.getUser(userID, part)
        })
        this.participantQuizListe = response;
        console.log(this.participantQuizListe);
      }
    })
  }

}
