import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit{

  user!: Utilisateur;

  constructor(private us: UtilisateurService){}

  ngOnInit(): void {
    if (sessionStorage.getItem("userConnected") != undefined) {
      this.user = JSON.parse(sessionStorage.getItem("userConnected")+"")
    }
  }

  save(){
    this.us.register(this.user).subscribe({
      next: (response) =>{
        console.log(response);
        
      },
      error: e=>{
        console.log(e);
        
      }
    })
  }

}
