import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/Utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit{

  users: Utilisateur[] = [];
  
  constructor(private readonly userService: UtilisateurService){}

  ngOnInit(): void {
    this.getUsers()
  }

  private getUsers(){
    this.userService.allusers().subscribe({
      next: (response) =>{
        this.users = response
        console.log(response);
      },
      error: (e) => {
        console.log(e);
        
      }
    })
  }

}
