import { AbstractEntity } from "./AbstractEntity";
import { Evaluation } from "./Evaluation";
import { Question } from "./Question";
import { Utilisateur } from "./Utilisateur";

export class Questionaire extends AbstractEntity{

     code! : string;

     titre! : string;

     description! : string;

     image! : string;

     utilisateur! : Utilisateur;

     questions! : Question[];

     evaluations! : Evaluation[];
}