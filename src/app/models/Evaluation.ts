import { Questionaire } from "./Questionaire";
import { Utilisateur } from "./Utilisateur";

export class Evaluation {

      id!: number;

      dateEval! : Date;

      score! : number;

      identify!: string;

      rang! : number;

      utilisateur! : Utilisateur;

      questionaire! : Questionaire;

      getIdentify(){
            this.identify = `${this.utilisateur.id}-${this.questionaire.id}`;
      }

}