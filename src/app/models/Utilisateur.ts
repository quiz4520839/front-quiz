import { AbstractEntity } from "./AbstractEntity";
import { Evaluation } from "./Evaluation";
import { Questionaire } from "./Questionaire";
import { Roles } from "./Roles";

export class Utilisateur extends AbstractEntity{

     nom! : string;

     prenom! : string;

     username! : string;

     email! : string;

     password! : string;

     num! : string;

     sexe! : string;

     photo! : string;

     evaluationList! : Evaluation[];

     questionaires! : Questionaire[];

     roles! : String[];
}