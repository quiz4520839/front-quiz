import { AbstractEntity } from "./AbstractEntity";
import { Questionaire } from "./Questionaire";
import { Response } from "./Response";

export class Question extends AbstractEntity{

     libelle! : string;

     responses: Response[] = [];

     questionaire! : Questionaire;
}