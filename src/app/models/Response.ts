import { AbstractEntity } from "./AbstractEntity";

export class Response{

     id! : number;

     libelle! : string;

     exactly : Boolean = false;

     constructor(id: number){
          this.id = id;
     }

}