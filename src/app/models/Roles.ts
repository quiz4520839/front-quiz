import { AbstractEntity } from "./AbstractEntity";
import { Utilisateur } from "./Utilisateur";

export class Roles extends AbstractEntity{

     rolename! : string;

     utilisateur! : Utilisateur;
}