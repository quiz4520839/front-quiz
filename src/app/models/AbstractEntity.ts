export class AbstractEntity{

    id! : number;

    creationDate! : Date;

    lastModifiedDate! : Date;
}